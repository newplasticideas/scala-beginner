package lectures.part1basics

object ValuesVariablesTypes extends App {
  val x = "Hello scala"
  println(x)
  //  x = 2
  // VALS ARE IMMUTABLE
  // types are optional. the compiler can infer types.

  val aString: String = "hello"
  val anotherString = "goodbye"

  val aBoolean: Boolean = false
  val aChar:Char = 'a'
  val aInt: Int = x
  val aShort: Short = 4613
  // short is 2 bytes. long is 4 bytes.
  val aLong: Long = 12345678L // put an L at the end of a Long to indicate Long as opposed to Int
  val aFloat: Float = 2.0f // f at the end of Float
  val aDouble: Double = 3.14

  // variables
  var aVariable: Int = 4
  aVariable = 5
  // side effects allow us to see what the program is doing.
  // programs without var are easier to keep track of because vars don't change.

  // SUMMARY
  // vals are immutable. cannot be reassigned.
  // vars are mutable. they can be reassigned.
  // vals are much preferred.
  // types can be inferred by the compiler.
  // bool, int, long, double and string have all been covered.
}
